package com.greenblat;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите температуру окружающей среды: ");
        double temperature = scanner.nextDouble();

        System.out.print("Введите скорость ветра (м/с): ");
        double speed = scanner.nextDouble();

        System.out.println("Введите вероятность выпадения снега или дождя (%): ");
        double precipitationProbability = scanner.nextDouble();

        if (temperature > 25 && precipitationProbability < 50) {
            if (speed > 10)
                System.out.println("Текущие погодные условия подходят для прогулки");
            else
                System.out.println("Текущие погодные условия НЕ подходят для прогулки, на улице высокая температура и низкая скорость ветра");
        } else if (temperature > 0 && temperature < 25 && speed < 10) {
            if (precipitationProbability < 50)
                System.out.println("Текущие погодные условия подходят для прогулки");
            else
                System.out.println("Температура для прогулки подходящая, однако высока вероятность дождя - возьмите зонт");
        } else if (temperature < 0 && temperature > -20 && precipitationProbability < 50) {
            if (speed < 5)
                System.out.println("Текущие погодные условия подходят для прогулки");
            else
                System.out.println("Текущие погодные условия НЕ подходят для прогулки, на улице низкая температура и высокая скорость ветра");
        } else {
            System.out.println("Текущие погодные условия НЕ подходят для прогулки");
        }

    }
}